# frozen_string_literal: true

module Styling
  class Configuration < WidgetInstanceConfiguration
      attribute :width, :integer, default: 1
      attribute :style, :string, default: "solid"
      attribute :alignment, :string, default: "center"

      validates :style, inclusion: %w[solid dotted inset outset dashed double groove ridge]
      validates :alignment, inclusion: %w[top center bottom]
  end
end
